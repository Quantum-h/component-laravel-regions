# Add regions and regionable behaviour to a Laravel app

## Credits

- [Carlos Rodriguez]
- [David Vazquez]

## Installation

You can install the package via composer:

``` bash
composer require quamtumh/laravel-regions
```

The package will automatically register itself.

You can publish the migration with:
```bash
php artisan vendor:publish --provider="Quantumh\Regions\RegionsServiceProvide" --tag="tags-migrations"
```

After the migration has been published you can create the `regions` and `regionables` tables by running the migrations:

```bash
php artisan migrate
```

You can optionally publish the config file with:
```bash
php artisan vendor:publish --provider="Quantumh\Regions\RegionsServiceProvide" --tag="tags-config"
```


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information
