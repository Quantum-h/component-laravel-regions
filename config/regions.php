<?php

return [

    /*
     * The fully qualified class name of the region model.
     */
    'region_model' => Quantumh\Regions\Region::class,
];
