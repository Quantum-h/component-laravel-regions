<?php

namespace Quantumh\Regions;

use ArrayAccess;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


trait HasRegions
{
    protected array $queuedRegions = [];

    public static function getRegionClassName(): string
    {
        return config('regions.region_model', Region::class);
    }

    public static function bootHasRegions()
    {
        static::addGlobalScope(new RegionScope);

        static::created(function (Model $regionableModel) {
            if(session()->has('region_id')) {
                $currentRegionId = DB::table('regions')->where('id', session()->get('region_id'))->first()->id;
            }else{
                $currentRegionId = DB::table('regions')->first()->id;
            }
            $regionableModel->syncRegions([$currentRegionId]);
        });

    }

    public function regions(): MorphToMany
    {
        return $this
            ->morphToMany(self::getRegionClassName(), 'regionable');
    }

    public function syncRegions(array|ArrayAccess $regions): static
    {
        $className = static::getRegionClassName();

        $regions = collect($className::find($regions));

        $this->regions()->sync($regions->pluck('id')->toArray());

        return $this;
    }
}
