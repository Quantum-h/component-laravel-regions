<?php

namespace Quantumh\Regions;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;


class Region extends BaseModel
{
    use HasFactory;

    const PERMISSION_SUFFIX = 'regions';

    public $guarded = [];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $user = auth()->user();
            if($user) {
                $user->regions()->attach($model->id);
            }
        });

        static::addGlobalScope(new RegionScope);
    }
}
