<?php

namespace Quantumh\Regions;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class RegionScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if ($model instanceof User) return;

        $user = auth()->guard('web')->user();

        if ($user) {
            //We are using a custom method instead of relation $user->regions to avoid infinity loop (because regions is checking itself)
            if ($model->getTable() == Region::PERMISSION_SUFFIX) {
                $regions = $this->getUserRegions($user->id);
                if (count($regions)) {
                    $builder->whereIntegerInRaw('regions.id', $regions);
                }
            } else {
                $regionId = session()->has('region_id') ? session()->get('region_id') : $this->getUserRegions($user->id)->first();

                $builder
                    ->whereHas('regions', function (Builder $query) use ($regionId) {
                        $query->where('regions.id', $regionId);
                    });
            }
        } else {
            if ($model->getTable() != Region::PERMISSION_SUFFIX) {
                $regionId = session()->has('region_id') ? session()->get('region_id') : Region::first()->id;
                $builder
                    ->whereHas('regions', function (Builder $query) use ($regionId) {
                        $query->where('regions.id', $regionId);
                    });
            }
        }
    }

    public function getUserRegions($id)
    {
        return DB::table('regionables')->where('regionable_id', $id)->where('regionable_type', 'App\Models\User')->get()->pluck('region_id');
    }
}

