<?php

namespace Quantumh\Regions;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class RegionsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-regions')
            ->hasConfigFile()
            ->hasMigration('create_region_tables');
    }
}
