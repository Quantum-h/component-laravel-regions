<?php

namespace Quantumh\Regions;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserRegionScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if(session()->has('region_id')) {
            if ($model->getTable() == User::PERMISSION_SUFFIX) {
                $builder->with('actualRegion')->whereHas('actualRegion');
            }
        }
    }
}
